/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, View, Button, Dimensions, Image} from 'react-native';
import {captureDocument, captureSelfie} from 'react-native-okaycam';

const license = Platform.select({
  android: '2xdNOk6kijI_P8Vgm0UX4xLdwglhQz348wysrlPINFjkAA9rxRcZLfgdLxO2vjuZ',
  ios: '2xdNOk6kijI_P8Vgm0UX4xLdwglhQz348wysrlPINFjkAA9rxRcZLfgdLxO2vjuZ',
});

const screenWidth = Dimensions.get('screen').width * 0.9;
const screenHeight = (screenWidth * 54) / 85;

// const refImg = require('./images/mykad.svg');
const refImg = Image.resolveAssetSource(require('./images/mykad.svg')).uri;
console.log('ref img : ', refImg);

const App = () => {
  const captureIDImage = () => {
    captureDocument(license, false, {
      topLabel: {
        text: 'Align your card within the frame',
        color: '#4287f5',
        size: 20,
      },
      bottomLabel: {
        text: 'Tap to Focus',
        color: '#4287f5',
        size: 20,
      },
      frame: {
        size: {
          width: screenWidth,
          height: screenHeight,
        },
        color: '#4287f5',
        // content: require('./images/mykad.svg'),
      },
      showOverlay: false,
      timer: {
        backgroundColor: '#4287f5',
        textColor: '#ffffff',
      },
      torchBtnEnabled: true,
      confirmBtnConfig: {
        backgroundColor: '#4287f5',
        contentColor: '#000000',
      },
      retakeBtnConfig: {
        backgroundColor: '#4287f5',
        contentColor: '#000000',
      },
      captureBtnColor: '#4287f5',
      firstPhotoConfig: {
        delay: 0,
        onFlash: false, 
        onImageQuality: true,
        outputPath: null,
      },
      secondPhotoConfig: {
        delay: 3,
        onFlash: false, 
        onImageQuality: true,
        outputPath: null,
      },
      crop: true,
      width: 2000,
      imageQuality: 0.5,
      // preview: {
      //   title: {text: 'testing', color: '#000000', size: 25},
      //   refImage: refImg,
      //   instruction1: {text: 'instr1', color: '#000000', size: 14},
      //   instruction2: {text: 'instr2', color: '#000000', size: 14},
      //   instruction3: {text: 'instr3', color: '#000000', size: 14},
      //   backgroundColor: '#374fff',
      // },
      preview: null,
      // instruction: {
      //   title: {text: 'instr1', color: '#ffffff', size: 14},
      //   refImage1: {
      //     title: {text: 'ref1', color: '#ffffff', size: 14},
      //     img: refImg,
      //   },
      //   refImage2: {
      //     title: {text: 'ref2', color: '#ffffff', size: 14},
      //     img: refImg,
      //   },
      // },
      instruction: null,
    })
      .then(result => {
        console.log(result);
      })
      .catch(error => {
        console.log(error);
      });
  };

  captureSelfieImage = () => {
    captureSelfie(license, true, {
      topLabel: {
        text: 'Align you face within the frame',
        color: '#4287f5',
        size: 20,
      },
      bottomFrameColor: '#4287f5',
      captureBtnColor: '#ffffff',
      switchBtnConfig: {
        color: '#ffffff',
        show: true,
      },
      confirmBtnConfig: {
        backgroundColor: '#4287f5',
        contentColor: '#000000',
      },
      retakeBtnConfig: {
        backgroundColor: '#4287f5',
        contentColor: '#000000',
      },
      defaultCameraFacing: 'front',
      width: 1000,
      imageQuality: 0.5,
    })
      .then(result => {
        console.log(result);
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <View style={styles.containerStyle}>
      <Image
        source={require('./images/logo.png')}
        style={{width: 40, height: 40}}
      />
      <View style={styles.btnContainer}>
        <Button title="Capture ID Image" onPress={captureIDImage} />
      </View>

      <View style={styles.btnContainer}>
        <Button title="Capture Selfie Image" onPress={captureSelfieImage} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    padding: 20,
  },
  btnContainer: {
    marginTop: 50,
  },
});

export default App;
